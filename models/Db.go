package models

import (
	"github.com/jinzhu/gorm"
	//pq
	_ "github.com/lib/pq"
)

//UserDet holds the user data
type UserDet struct {
	ID    uint
	Name  string
	Email string
}

// DB is Reference to db
var DB *gorm.DB

//ConnectDB establishes connection
func ConnectDB() {

	db, err := gorm.Open("postgres", "host=localhost port=5432 user=zopsmart password=zopsmart dbname=apiapp sslmode=disable")

	if err != nil {
		panic(err)
	}

	db.SingularTable(true)

	db.DropTableIfExists(&UserDet{})
	db.CreateTable(&UserDet{})

	DB = db
}

//CreateUsers adds user details to database
func CreateUsers() {

	user1 := UserDet{Name: "Vageesha", Email: "vageesha@zopsmart.com"}
	user2 := UserDet{Name: "Arun", Email: "arunnasarain@gmail.com"}
	user3 := UserDet{Name: "Ajay", Email: "ajay@gmail.com"}
	user4 := UserDet{Name: "Abhi", Email: "abhi@gmail.com"}
	user5 := UserDet{Name: "Sakshi", Email: "sakshi@gmail.com"}

	DB.Create(&user1)
	DB.Create(&user2)
	DB.Create(&user3)
	DB.Create(&user4)
	DB.Create(&user5)

}

/////////////////////////////////////////////////////

//GetUsers fetches all users
func GetUsers() ([]UserDet, error) {

	var users []UserDet

	err := DB.Find(&users).Error

	return users, err

}

//OneUser fetches details of one user
func OneUser(id string) (UserDet, error) {

	var user UserDet

	err := DB.Where("id =?", id).Find(&user).Error

	return user, err

}

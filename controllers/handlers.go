package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/arunnasarain008/api-users/models"
)

//GetUsers fetches all users
func GetUsers(c *gin.Context) {

	users, err := models.GetUsers()

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, gin.H{
		"data": users,
	})

}

//OneUser fetches the user details of given id
func OneUser(c *gin.Context) {

	id := c.Param("id")

	user, err := models.OneUser(id)

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, gin.H{
		"data": user,
	})

}

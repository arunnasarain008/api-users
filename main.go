package main

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/arunnasarain008/api-users/controllers"
	"gitlab.com/arunnasarain008/api-users/models"
)

func main() {

	r := setup()

	r.Run()

	defer models.DB.Close()

}

func setup() *gin.Engine {
	r := gin.Default()

	models.ConnectDB()
	models.CreateUsers()

	r.GET("/users", controllers.GetUsers)
	r.GET("/users/:id", controllers.OneUser)

	return r
}
